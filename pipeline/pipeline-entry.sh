#!/bin/bash -x

# In order to find out what tools have been modified by a change, the type of
# event triggering the CI/CD pipeline must be identified, so that the
# appropriate git command can be used to compare the proposed change versus the
# master branch and find out what has been modified.
DELTA_SRC=''
echo ${CI_COMMIT_MESSAGE} | grep -q "See merge request ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}"
if [ $? -eq 0 -a ${CI_PIPELINE_SOURCE} == 'push' ]; then
  echo "CI/CD triggered by a merge event."
  DELTA_SRC='merge'
else
  if [ ${CI_MERGE_REQUEST_IID}"" != "" -a ${CI_PIPELINE_SOURCE} == 'merge_request_event' ]; then
    echo "CI/CD triggered by a merge request event."
    DELTA_SRC='merge_request'
  else
    echo "Not a merge request nor a merge, so nothing for CI/CD to do. Exiting."
    exit 0
  fi
fi

# If the event is a merge then the SVC changes can be found by comparing the
# difference between the two commit IDs that are supplied by the CI. Otherwise,
# if it is a merge request, then the changes can be found by comparing the HEAD
# with origin/master.
if [ ${DELTA_SRC} == 'merge' ]; then
  DELTA_BEFORE_TAG=${CI_COMMIT_BEFORE_SHA}
  DELTA_TAG=${CI_COMMIT_SHA}
elif [ ${DELTA_SRC} == 'merge_request' ]; then
  DELTA_BEFORE_TAG="origin/master"
  DELTA_TAG="HEAD"
else
  echo "Invaid change source ${DELTA_SRC}. Exiting."
  exit 1
fi

# Find out what tools have been modified in this change.
TOOLS_MODIFIED=$(git diff ${DELTA_BEFORE_TAG} ${DELTA_TAG} --name-only | grep "^tools/" | cut -d'/' -f2 | sort | uniq)

# If nothing changed, exit the job immediately. Otherwise, run the current
# stage of the pipeline for the tool(s) modified. The environment variable
# ${CI_JOB_STAGE} is defined by Gitlab CI.
#
# There are three stages defined at the moment:
# - build: runs the docker-build.sh script against the modified ${TOOL},
#   creating a container image for it and saving the image to the local
#   filesystem.
#
# - test: reads the image from the local filesystem, creates a container from
#   it, and runs the tests defined to validate it works.
#
# - deploy: reads the image from the local filesystem, pushes it to Docker Hub.
#
if [[ ${TOOLS_MODIFIED} == "" ]]; then
  echo "No tools modified. Nothing to do."
  exit 0
else
  for TOOL in ${TOOLS_MODIFIED}; do
    bash -x "pipeline/stage-${CI_JOB_STAGE}.sh" "${TOOL}"
    if [ $? -eq 0 ]; then
      echo "Stage ${CI_JOB_STAGE} for ${TOOL} succeeded."
    else
      echo "Stage ${CI_JOB_STAGE} for ${TOOL} failed. Exiting."
      exit 1
    fi
  done
fi

exit 0
